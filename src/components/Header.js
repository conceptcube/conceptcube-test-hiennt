import logoIcon from '../asset/images/logo-icon.png';
import searchIcon from '../asset/images/white-search-icon.png';
import '../App.css';
const Header = () => {
  return (
    <nav id="menu">
      <ul>
        <li className="logo">
          <img src={logoIcon} className="logo_icon" />
        </li>
        <li>
          <a href="#">Discover</a>
        </li>
        <li>
          <a href="#">Job</a>
        </li>
        <li className="search">
          <input placeholder="Search for motion trend......" />
        </li>
        <li>
          <button>
            <img src={searchIcon} className="white_search_icon" />
          </button>
        </li>
        <li>
          <a href="#">Login</a>
        </li>
        <li>
          <a href="#">Sign Up</a>
        </li>
      </ul>
    </nav>
  );
};

export default Header;
