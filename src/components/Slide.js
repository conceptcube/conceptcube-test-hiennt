import '../App.css';
import leftArrow from '../asset/images/l-red-circle-arrow.png';
import rightArrow from '../asset/images/r-red-circle-arrow.png';
const Slide = () => {
  return (
    <div id="slide">
      <div className="wrapper">
        <div className="item">
          <span className="textStyle14">title</span>
        </div>
        <div className="item">
          <span className="textStyle14">Editing</span>
        </div>
        <div className="item">
          <span className="textStyle14">Camera Action, Angle</span>
        </div>
        <div className="item">
          <span className="textStyle14">Sound, Beat</span>
        </div>
        <div className="item">
          <span className="textStyle14">Graphical</span>
        </div>
        <div className="item selected">
          <span className="textStyle14">Experimental</span>
        </div>
        <div className="item">
          <span className="textStyle14">Elements</span>
        </div>
        <div className="item">
          <span className="textStyle14">Car</span>
        </div>
        <div className="item">
          <span className="textStyle14">Gun</span>
        </div>
        <div className="item">
          <span className="textStyle14">title</span>
        </div>
      </div>
      <img className="left-arrow icon-arrow" src={leftArrow} />
      <img className="right-arrow icon-arrow" src={rightArrow} />
    </div>
  );
};

export default Slide;
