import Content from './Content';
import Footer from './Footer';
import Header from './Header';
const Main = () => {
  return (
    <div>
      <Header />
      <Content />
      <Footer />
    </div>
  );
};
export default Main;
