import '../App.css';
import Slide from './Slide';
const Content = () => {
  return (
    <section className="main">
      <Slide />
    </section>
  );
};

export default Content;
