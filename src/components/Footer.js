import '../App.css';
import blackLogoIcon from '../asset/images/black-logo-icon.png';
const Footer = () => {
  return (
    <footer>
      <div className="contact-container">
        <div className="contact-item">
          <span className="textStyle15">Contact</span>
          <span>motiontrend@test.com</span>
        </div>
        <div className="contact-item">
          <span>@ Motiontrend. All rights reserved.</span>
          <span className="socialIcon">
            <i className="fa fa-instagram" aria-hidden="true"></i>
            <i className="fa fa-facebook" aria-hidden="true"></i>
            <i className="fa fa-youtube-play" aria-hidden="true"></i>
          </span>
        </div>
      </div>
      <div className="introduction">
        <span className="introduction-links">
          <a href="#">Introduction</a>
          <a href="#">Terms of Service</a>
          <a>
            <select className="selectLanguage">
              <option value="" disabled="disabled" selected="selected">
                Language
              </option>
              <option>English</option>
              <option>Korean</option>
              <option>Vietnamese</option>
            </select>
          </a>
          <a href="#">Customer Service</a>
        </span>
        <img src={blackLogoIcon} className="blacklogo_icon" />
      </div>
    </footer>
  );
};

export default Footer;
